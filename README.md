# Practical work

Task is to create a simple pipeline using GitLab CI/CD. Pipeline should lint and test python code that is in the repository. Follow the next instructions:

1. Fork this repository.
2. Create CI file.
3. For default image use `python:3.8`.
4. Create two stages each containing one job.
5. First job should be linting one and you should install `pylint` and `pytest` in order for it to pass. Lint both python files.
6. Second job should be test one. You should install `pytest` in order for it to run and run it on `test_wallet.py`.
7. Both jobs should be run only on master.
8. Both jobs should be run in every case except when you have done some changes on `README.md`.
